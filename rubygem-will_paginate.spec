# Generated from will_paginate-2.3.15.gem by gem2rpm -*- rpm-spec -*-
%global gem_name will_paginate
%if 0%{?el6}
%global rubyabi 1.8
%else
%global rubyabi 1.9.1
%endif

Summary: Pagination for Rails
Name: rubygem-%{gem_name}
Version: 2.3.15
Release: 1%{?dist}
Group: Development/Languages
License: MIT
URL: http://github.com/mislav/will_paginate/wikis
Source0: %{gem_name}-%{version}.gem
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
BuildRequires: ruby(abi) = %{rubyabi}
%if 0%{?fedora}
BuildRequires: rubygems-devel
%else
BuildRequires: ruby(rubygems)
%endif
# Tests:
BuildRequires: rubygem(minitest)
BuildRequires: rubygem(mocha)
BuildRequires: rubygem(actionpack) < 3
BuildRequires: rubygem(activerecord) < 3
%if 0%{?el6}
BuildRequires: rubygem(sqlite3-ruby)
%else
BuildRequires: rubygem(sqlite3)
%endif
BuildArch: noarch
Provides: rubygem(%{gem_name}) = %{version}

# macros for RHEL6 compatibility:
%{!?gem_dir: %global gem_dir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)}
%{!?gem_instdir: %global gem_instdir %{gem_dir}/gems/%{gem_name}-%{version}}
%{!?gem_libdir: %global gem_libdir %{gem_instdir}/lib}
%{!?gem_cache: %global gem_cache %{gem_dir}/cache/%{gem_name}-%{version}.gem}
%{!?gem_spec: %global gem_spec %{gem_dir}/specifications/%{gem_name}-%{version}.gemspec}
%{!?gem_docdir: %global gem_docdir %{gem_dir}/doc/%{gem_name}-%{version}}
%{!?gem_extdir: %global gem_extdir %{_libdir}/gems/exts/%{gem_name}-%{version}}

%description
The will_paginate library provides a simple, yet powerful and extensible API
for ActiveRecord pagination and rendering of pagination links in ActionView
templates.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}

%prep
%setup -q -c -T
mkdir -p .%{gem_dir}
gem install --local --install-dir .%{gem_dir} \
            --force %{SOURCE0}

%build

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/

%check
pushd .%{gem_instdir}
  testrb -Ilib test/*_test.rb
popd


%files
%dir %{gem_instdir}
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/README.rdoc
%doc %{gem_instdir}/LICENSE
%doc %{gem_instdir}/CHANGELOG.rdoc
%{gem_instdir}/Rakefile
%{gem_instdir}/test

%changelog
* Sat Feb 16 2013 ktdreyer@ktdreyer.com - 2.3.15-1
- Initial package, using gem2rpm 0.8.1
